package com.iws.isobar.awarenessapi;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.android.gms.awareness.Awareness;
import com.google.android.gms.awareness.fence.AwarenessFence;
import com.google.android.gms.awareness.fence.DetectedActivityFence;
import com.google.android.gms.awareness.fence.FenceState;
import com.google.android.gms.awareness.fence.FenceUpdateRequest;
import com.google.android.gms.awareness.fence.HeadphoneFence;
import com.google.android.gms.awareness.state.HeadphoneState;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity {

    private static final String FENCE_RECEIVER_ACTION =
            "com.iws.isoba.awarenessapi";
    private PendingIntent pendingIntent;
    private FenceReveiver myFenceReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(FENCE_RECEIVER_ACTION);

        //Pending intent that will be used to get callbacks from fence
        pendingIntent = PendingIntent.getBroadcast(this, 0, intent , 0);
        myFenceReceiver = new FenceReveiver();
        registerReceiver(myFenceReceiver, new IntentFilter(FENCE_RECEIVER_ACTION));

        //Creating the Awareness Client
        GoogleApiClient client = new GoogleApiClient.Builder(this)
                .addApi(Awareness.API)
                .build();
        client.connect();

    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            //unregister receiver in case it makes sense
            //unregisterReceiver(myFenceReceiver);
        }
        catch (IllegalArgumentException e ) {
        }

    }



    public class FenceReveiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            //Here is where we will receive the notifications from Fence API

            FenceState fenceState = FenceState.extract(intent);
            if (fenceState.getFenceKey().equals("someFence")) {
                switch (fenceState.getCurrentState()) {
                    case FenceState.TRUE:
                        Toast.makeText(MainActivity.this, "SOMETHING JUST HAPPENED!", Toast.LENGTH_LONG).show();
                        break;
                }
            }
        }
    }

}
